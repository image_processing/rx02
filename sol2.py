import numpy as np
from scipy import misc, signal
from skimage import color

PI = np.pi
RGB_CHANNELS = 3
GRAYS_SCALE = 1
COLOR = 2


def read_image(filename, representation):
    """
    :param filename - string containing the image filename to read.
    :param representation - representation code, either 1 or 2 defining whether the output should be a grayscale 
    :return:  an image, make sure the output image is represented by a matrix of type
    np.float64 with intensities (either grayscale or RGB channel intensities) normalized to the range [0, 1].
    """

    pic = np.float64(misc.imread(filename, mode='RGB') / 255)
    if representation == GRAYS_SCALE:
        return color.rgb2gray(pic).astype(np.float64)
    else:
        return pic


def DFT(signal):
    """
    # create matrices with the shape
    # u[0]x[0]  |  u[0]x[1]  |  u[0]x[2]  |... |  u[0]x[N-1]
    # u[1]x[0]  |  u[1]x[1]  |  u[1]x[2]  |... |  u[1]x[N-1]
    #   .             .             .                 .
    #   .             .             .                 .
    # u[N-1]x[0]  |  u[N-1]x[1]  |  u[N-1]x[2]  |... |  u[N-1]x[N-1]
    #
    # then mult with the transform values (-2iπux/N) and exponent
    # then use dot product to get wanted result:  mat*signal
    
    :param signal: an array of dtype float64 with shape (N,1) 
    :return: the complex Fourier signal
    """

    N, _ = signal.shape
    xs = np.arange(N)
    us = xs.reshape(-1, 1)
    vals_mat = xs * us
    func_mat = np.exp(-2j * PI * vals_mat / N)

    return np.dot(func_mat, signal).astype(np.complex128)


def IDFT(fourier_signal):
    """
    Work with the same mathod as DFT
    :param fourier_signam:  an array of dtype complex128  with shape (N,1) 
    :return: the complex signal
    """
    N, _ = fourier_signal.shape
    xs = np.arange(N)
    us = xs.reshape(-1, 1)
    vals_mat = xs * us
    func_mat = np.multiply((1 / N), np.exp(2j * PI * vals_mat / N))
    return np.dot(func_mat, fourier_signal).astype(np.complex128)


def DFT2(image):
    """
    calculate the DFT of a grayscale image.
    As suggested in instruction, First apply on rows and then on columns 
    :param image: a grayscale image of dtype float64
    :return: 
    """
    return DFT(DFT(image).T).T


def IDFT2(fourier_image):
    """
    
    :param fourier_image: a 2D array of dtype complex128 
    :return: 
    """
    return IDFT(IDFT(fourier_image).T).T


KERNEL = np.array([1, 0, -1]).reshape(1, -1)


def conv_der(im: np.float64):
    """
    
    :param im: grayscale images of type float64, boundary='wrap'
    :return: grayscale the magnitude of the derivative of image, type float64
    """
    dx = signal.convolve2d(im, KERNEL, mode='same', boundary='wrap')
    dy = signal.convolve2d(im, KERNEL.reshape(-1, 1), mode='same', boundary='wrap')

    return np.sqrt(np.abs(dx) ** 2 + np.abs(dy) ** 2)


def fourier_der(im):
    """
    Will follow the algorithm:
    • To	compute	the	x	derivative	of		f	(up	to	a	constant):
        – Compute	the	Fourier	transform	F
        – Multiply	each	Fourier	coefficient	F(u,v)	by	u
        – Compute	the	inverse	Fourier	transform
    • To	compute	the	y	derivative	of		f	(up	to	a	constant):
        – Compute	the	Fourier	transform	F
        – Multiply	each	Fourier	coefficient	F(u,v)	by	v
        – Compute	the	inverse	Fourier	transform
    :param im: float64 grayscale image
    :return: float64 grayscale image - The magnitude of the image deriavtives using Fourier transform
    """
    # set the variables for the calculation later. get M/2 and N/2 for range - later make center = 0 using fftshift
    M_half = im.shape[0] // 2
    N_half = im.shape[1] // 2
    us = np.arange(-M_half, M_half).reshape(-1, 1)
    vs = np.arange(-N_half, N_half).reshape(-1, 1)

    # make 0 the center
    f_transform_centered = np.fft.fftshift(DFT2(im))

    dx = IDFT2(f_transform_centered * us) * 2*PI*1j/im.shape[0]
    dy = IDFT2(f_transform_centered * vs.T)* 2*PI*1j/im.shape[1]

    return np.sqrt(np.abs(dx) ** 2 + np.abs(dy) ** 2)


def blur_spatial(im: np.float64, kernel_size: int):
    """
    
    :param im: the input image to be blurred (grayscale float64 image).
    :param kernel_size:  the size of the gaussian kernel in each dimension (an odd integer).
    :return: blurry image (grayscale float64 image) using gaussian kernel
    """

    kernel = get_kernel(kernel_size)
    # normelize kernel to sum up to 1 exactly


    return signal.convolve2d(im, kernel, mode='same', boundary='wrap')


def blur_fourier(im, kernel_size):
    """

    :param im: the input image to be blurred (grayscale float64 image).
    :param kernel_size:  the size of the gaussian kernel in each dimension (an odd integer).
    :return: blurry image (grayscale float64 image) using gaussian kernel
    """
    kernel = get_kernel(kernel_size)

    h_diff, w_diff = im.shape[0] - kernel_size, im.shape[1] - kernel_size
    h1_pad, h2_pad = int(np.ceil(h_diff / 2)), int(np.floor(h_diff / 2))
    w1_pad, w2_pad = int(np.ceil(w_diff / 2)), int(np.floor(w_diff / 2))

    # Padding kernel with 0 in such way that the orig kernel will be at (floor(m/2) + 1 floor(n/2) + 1)
    pad_kernel = np.pad(kernel, ((h1_pad, h2_pad), (w1_pad, w2_pad)), "constant")

    fourier_kernel = DFT2(np.fft.ifftshift(pad_kernel))

    # shigting img to match the centered kernel
    four_img = DFT2(im)

    four_mult_res = np.multiply(fourier_kernel, four_img)
    return IDFT2(four_mult_res).astype(np.float64)


def get_kernel(kernel_size, normalized=True):
    """
    Get a a gaussian kernel
    With the promise for kernel_size to be odd number. this will build the appropriate
    gaussian kernel
    :param normalized: 
    :param basic_kernel: 
    :param kernel_size: 
    :return: The normalized gaussian kernel
    """
    if kernel_size == 1:
        # returning 1X1 Kernal
        return np.array([[1]])
    basic_kernel = np.array([[1, 1], [1, 1]], dtype=np.uint64)
    kernel = basic_kernel.copy()
    while kernel.shape[0] * 2 < kernel_size:
        kernel = signal.convolve(kernel, kernel)
    while kernel.shape[0] < kernel_size:
        kernel = signal.convolve(kernel, basic_kernel)

    return kernel / np.sum(kernel) if normalized else kernel
